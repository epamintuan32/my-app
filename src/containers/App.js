import React, { Component } from 'react';
/*import {Responsive} 
import Navbar from '../components/Navbar'
import './App.css';*/


class App extends Component{
	constructor(props){
		super(props);
		this.state = {
			user: [],
			id: '',
			name: '',
			age: '',
			address: ''
		};

		
	}

	onhandleSubmit (e){
		e.preventDefault();

		const {age, name, address} = this.state;
		this.setState({
			user : this.state.user.concat({name,age,address}) 
		});
		// console.log(name);
		// console.log(age);
		// // console.log(address);	
		// console.log(this.state.user);
	}

	renderUser(){
		// console.log(this.state.user);
		return this.state.user.map( user => {
				return(
					<div key = {Math.random()}>
						<p> - {user.name}</p>
						<p> - {user.age}</p>
						<p> - {user.address}</p>
					</div>						
				)
			})
	}


	render(){
		return(
			<div>
				<form onSubmit={this.onhandleSubmit.bind(this)}>
					<h2> Please input User name</h2>
					<input type="text" value={this.state.name} onChange={(e) => this.setState({name:e.target.value})} placeholder="Name"/><br></br>
					<input type="text" value={this.state.age} onChange={(e) => this.setState({age:e.target.value})} placeholder="Age"/><br></br>
					<input type="text" value={this.state.address} onChange={(e) => this.setState({address:e.target.value})} placeholder="Address"/><br></br>
					<input type="submit" value="Submit" />
				</form>
				<div>
					{this.renderUser()}
				</div>
			</div>
		);
	}
}

export default App;