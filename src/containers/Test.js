import React, {Component} from 'react';
import {Segment,  
		Input, 
		Button, 
		Responsive, 
		Grid
} from 'semantic-ui-react';

import Songlist from '../components/Songlist'

class Test extends Component {
	constructor(props){
		super(props);
		this.state = {
			songs: [],
			title: '',
			artist: '',
		}

	}

	onhandleSubmit(e){
		e.preventDefault();

		const {title, artist} = this.state;

		this.setState({
			songs: this.state.songs.concat({title, artist})
		});
	}

	render(){
		return (

			<div>
				<Responsive className="headerInput" inverted as={Segment}>
					<Segment inverted>
						<form onSubmit={this.onhandleSubmit.bind(this)}>
							<label>Please input a song: </label>
							
							<Input focus className="inputText" 
							type="text" value={this.state.title} 
							onChange={(e) => this.setState({title:e.target.value})} 
							placeholder="Song title"/>
							
							<Input focus className="inputText" 
							type="text" value={this.state.artist} 
							onChange={(e) =>  this.setState({artist:e.target.value})} 
							placeholder="Song Artist"/>
							
							<Button basic color='green' type="submit">Add</Button>
						</form>
					</Segment>
				</Responsive>
				<div className="main_content">
					<Grid relaxed>
						<Grid.Row>
								<Songlist className="" songs={this.state.songs}/>
						</Grid.Row>
					</Grid>
					
				</div>
			</div>
		);
	}
}

export default Test;