import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import './index.css';

// import App from './containers/App';
import Test from './containers/Test'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Test/>, document.getElementById('root'));
registerServiceWorker();	