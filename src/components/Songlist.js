import React, {Component} from 'react';
import UpdateButton from './updateModal';

import {
	Button,
	Card, 
	Grid
} from 'semantic-ui-react';

class Songlist extends Component{


	render(){
		console.log(this.props.songs)
		return this.props.songs.map( (song, index) => {
			return(
				<div className="gridSong" key = {index}>
					<Grid.Column>
						<Card.Group>
							<Card>
								<Card.Content>
									<Card.Header>{index + 1}. {song.title}</Card.Header>
									<Card.Description>{song.artist}</Card.Description>
								</Card.Content>
							  	<Card.Content extra>
							        <div className='ui two buttons'>
							        	<UpdateButton song={song} id={index} />
										<Button basic color='red'>
											Delete
										</Button>
							        </div>
						      	</Card.Content>
							</Card>
						</Card.Group>
					</Grid.Column>
				</div>
			)
		})
	}
}

export default Songlist;