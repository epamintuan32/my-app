import React, {Component} from 'react';
import {
		Button, 
		Modal, 
		Input
	} from 'semantic-ui-react';

class UpdateMod extends Component{
	constructor(props){
		super(props);
		this.state = {
			id: this.props.id,
			title: this.props.song.title,
			artist: this.props.song.artist,
			modalOpen: false,
		};
	}

	handleClose(e){
		this.setState({modalOpen:false})
	}

	handleOpen(e){
		this.setState({modalOpen:true})
	}

	onhandleUpdate(e){
		e.PreventDefault();
		const {title, artist} = this.state;

		this.setState({title, artist});
	}

	render(){
		return (
			<Modal trigger={<Button basic color='yellow' 
				onClick={this.handleOpen.bind(this)}>Update</Button>}
				open={this.state.modalOpen}
				onClose={this.handleClose}>

				<Modal.Header>Updating Song #{this.props.id + 1}</Modal.Header>
				<Modal.Content>
					
					<form onSubmit={this.onhandleUpdate.bind(this)}>					
						<label>Song Title: </label><Input 
						fluid value={this.state.title} 
						onChange = {(e) => this.setState({title: e.target.value})}  
						placeholder='Song title'/><br></br>
						
						<label>Song Artist: </label><Input 
						fluid value={this.state.artist}
						onChange = {(e) => this.setState({artist: e.target.value})} 
						placeholder='Song artist'/>
						
						<Button color='green' type="button">Update</Button>
					</form>

				</Modal.Content>
				<Modal.Actions>
					<Button onClick={this.handleClose.bind(this)} 
					color='red' type="button">Cancel</Button>
				</Modal.Actions>
			</Modal>
		)
	}
}

export default UpdateMod;